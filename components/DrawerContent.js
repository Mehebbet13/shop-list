import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer'
import {CustomText} from "./CustomText";
import userImg from '../assets/placeholderAvatar.png'
import {connect} from "react-redux";
import {addUserCredentials, getCredentials} from "../store/project";
import COLORS from "../styles/colors";
const mapStateToProps = state => ({
    credentials: getCredentials(state)
})
export const DrawerContent =connect(mapStateToProps,{addUserCredentials})((props) => {
    const {image} = props.credentials;
    const {username} = props.credentials;
    const screenLinks = ['ONE TIME LIST','REGULAR LISTS', 'USER SETTINGS']
    return (
        <View style={{flex: 1, backgroundColor: COLORS.secondary}}>
            <DrawerContentScrollView>
                <View style={styles.userInfoSection}>
                    <Image style={styles.userImg} source={image===''?userImg:image}/>
                    <CustomText style={styles.name}>{username}</CustomText>
                </View>
            </DrawerContentScrollView>
            <View style={styles.drawerSection}>
                <TouchableOpacity
                    key={`ADD NEW LIST${Date.now()}`}
                    style={[styles.screenLink, styles.addNewList]}
                    onPress={() => {
                        props.navigation.navigate('CreateList')
                    }}
                ><CustomText weight={'bold'} style={styles.label}>ADD NEW LIST</CustomText>
                </TouchableOpacity>
                {screenLinks.map(screenLink => (
                    <TouchableOpacity
                        key={`${screenLink}${Date.now()}`}
                        style={styles.screenLink}
                        onPress={() => {
                            props.navigation.navigate(screenLink)
                        }}
                    ><CustomText weight={'bold'} style={styles.label}>{screenLink} </CustomText>
                    </TouchableOpacity>
                ))}


            </View>
        </View>
    );
});

const styles = StyleSheet.create({
    userInfoSection: {
        height: 90,
        width:260,
        display:'flex',
        flexDirection:'row',


    },
    name: {
        position: 'absolute',
        top: 40,
        left:80,
        display:'flex',
        justifyContent:'center',
        flexDirection:'column',
        fontStyle: 'normal',
        fontSize: 24,
        lineHeight: 25,
        color: '#303234'
    },
    drawerSection: {
        height: 530,
        backgroundColor:COLORS.main,
        alignItems: 'center',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    screenLink: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 55,
        paddingLeft: 55,
        backgroundColor: COLORS.secondary,
        borderRadius: 20,
        marginBottom: 20,
        color:COLORS.main
    },
    addNewList: {
        marginBottom: 40,
        marginTop: 30,
    },
    label: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        display: 'flex',
        alignItems: 'center',
        color:COLORS.main,

    },
    userImg: {
        width: 60,
        height: 60,
        marginLeft: 10,
        marginTop: 25,
        borderRadius: 29,
        borderWidth: 3,
        borderColor: COLORS.main

    }

});
