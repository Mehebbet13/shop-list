import React from 'react';
import {View, StatusBar, Image, TouchableOpacity, Keyboard} from 'react-native';
import {CustomText} from "./CustomText";
import {styles} from "../screens/HomeScreen/index";

export const ScreensBackground = ({title, children, image1, image2,onPress1, onPress2, ...rest}) => {
    return (
        <View style={styles.container}>
            <StatusBar translucent/>
            <View style={styles.pageContentNameContainer} {...rest}>
                <TouchableOpacity style={styles.image1} onPress={onPress1}>
                <Image source={image1}/>
                </TouchableOpacity>
                <CustomText weight={'bold'} style={styles.pageContentName}>{title}</CustomText>
                <TouchableOpacity style={styles.image2} onPress={onPress2}>
                    <Image source={image2}/>
                </TouchableOpacity>
            </View>
            <View style={styles.pageContent} onPress={()=>Keyboard.dismiss()}>
                {children}
            </View>
        </View>
    );
}

