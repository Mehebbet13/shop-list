import React,{useState} from 'react';
import { TouchableOpacity} from 'react-native';
import {CustomText} from "./CustomText";
import {ProgressBar} from "./ProgressBar";
import {styles} from "../screens/HomeScreen";

export const SingleItem = ({isRegular,onLongPress, listTitle, listProductsLength, onPress, listId,boughtItemsSize }) => {
    return (
        <TouchableOpacity activeOpacity={0.7}
                          style={[styles.listItem,{opacity:boughtItemsSize===0?1:isRegular?1:boughtItemsSize===listProductsLength?0.5:1}]}
                          id={listId}
                          onPress={()=> onPress(listId,isRegular,listTitle)}
                          onLongPress={()=> onLongPress()}
        >
            <CustomText style={styles.listItemTitle} weight={'bold'}>{listTitle}</CustomText>
            <CustomText
                style={styles.listItemPercentage}
                weight={'bold'}
            >
                {`${boughtItemsSize}`}/{`${listProductsLength}`}</CustomText>
            <ProgressBar done={boughtItemsSize===0?0:(boughtItemsSize / listProductsLength) * 100}/>
        </TouchableOpacity>
    );
};


