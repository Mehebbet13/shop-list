import React,{Component} from 'react';
import {
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {CustomText} from "./CustomText";

class ModalComp extends Component{

    render(){
    return (
        <View>

            <Modal
                animationType="slide"
                transparent={true}
            >
                <TouchableOpacity style={styles.centeredView} activeOpacity={0.9} onPress={() => this.props.hideModal()}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <CustomText>
                                Do you want to delete this list?
                            </CustomText>
                            <View style={styles.buttonsContainer}>
                                <TouchableHighlight activeOpacity={0.3}
                                    style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                    onPress={() => this.props.hideModal()}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    style={{...styles.openButton, backgroundColor: "#ff2c14"}}
                                   onPress={()=>this.props.handleDeleteList()}
                                >
                                    <Text style={styles.textStyle}>Delete</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Modal>

        </View>
    );
}}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    modalView: {
        height: 200,
        width: 260,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 45,
        shadowOpacity: 0.5,
        shadowRadius: 4,
        elevation: 100
    },
    buttonsContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20
    },
    openButton: {
        borderRadius: 15,
        padding: 15,
        elevation: 2,
        margin: 10
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
});

export default ModalComp;
