import React,{useState,useEffect} from 'react';
import {StyleSheet,View,Text} from 'react-native';
import COLORS from "../styles/colors";
export const ProgressBar = ({done}) => {
    const [newStyle, setNewStyle] = useState({});

    useEffect(() => {
        const newStyleAdd = {
            opacity: 1,
            width: `${done}%`
        }

        setNewStyle(newStyleAdd);
    }, [1]);

    return (
        <View style={styles.progress}>
            <View style={[styles.progressDone ,newStyle]} >

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
progress: {
    backgroundColor:COLORS.gray,
    borderRadius: 20,
    position: 'absolute',
    top: 45,
    left: 22,
    height: 20,
    width: 250,
},

progressDone: {
    backgroundColor:COLORS.yellow,
    borderRadius: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: 0,
    opacity: 0,
}

});
