const COLORS = {
    main: "#FF7676",
    secondary: "#FFFFFF",
    yellow: "#FFD976",
    lightYellow: "#FFE194",
    black: "#303234",
    gray: "#EEEEEE",

};

export default COLORS;
