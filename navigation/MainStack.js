import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {HomeScreen} from "../screens/HomeScreen";
import {CreateList, HomeScreen2, UserSettings} from "../screens";
import {SingleItemEdit} from "../screens/SingleItemEdit";
import {SingleList} from "../screens/SingleList";
import {SingleListEdit} from "../screens/SingleListEdit";
const { Navigator, Screen } = createStackNavigator();

const MainStack = () => {
    return (
        <Navigator>
            <Screen
                name="ONE TIME LIST"
                component={HomeScreen}
                options={{headerShown : false }}
            />
            <Screen
                name="REGULAR LISTS"
                component={HomeScreen2}
                options={{headerShown : false }}
            />
        <Screen
                name="CreateList"
                component={CreateList}
                options={{headerShown : false }}
            />
        <Screen
                name="SingleItemEdit"
                component={SingleItemEdit}
                options={{headerShown : false }}
            />
            <Screen
                name="SingleListEdit"
                component={SingleListEdit}
                options={{headerShown : false }}
            />
        <Screen
                name="SingleList"
                component={SingleList}
                options={{headerShown : false }}
            />
            <Screen
                name="USER SETTINGS"
                component={UserSettings}
                options={{headerShown : false }}
            />

        </Navigator>
    );
}

export default MainStack;

