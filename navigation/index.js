import React from 'react';
import {NavigationContainer} from '@react-navigation/native'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {DrawerContent} from "../components/DrawerContent";
import MainStack from "./MainStack";

const {Navigator, Screen}=createDrawerNavigator();
export const Drawer = () => {
    return (
<NavigationContainer>
           <Navigator  drawerContent={props => <DrawerContent {...props}/> }>
               <Screen name="Main" component={MainStack} />
           </Navigator>
</NavigationContainer>
    );
};

