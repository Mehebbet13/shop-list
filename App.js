import React, {Component} from 'react';
import {Drawer} from "./navigation";
import {AppLoading} from "expo";
import {loadFonts} from "./styles/fonts";
import {Provider} from "react-redux";
import {AsyncStorage, AppState, Text} from "react-native";
import {projectsReducer} from "./store/project";
import {createStore, combineReducers} from 'redux';

const rootReducers = combineReducers({
    projects: projectsReducer
});
let store = createStore(
    rootReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            store: store,
            isStoreLoading: true,
        }
    };

    retrieveData = async () => {
        try {
            await AsyncStorage.getItem('allData').then((value) => {
                let initialStore = JSON.parse(value);
                this.setState({
                    store: createStore(
                        rootReducers, initialStore,
                        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
                    )
                });
                this.setState({isStoreLoading: false})

            })
        } catch (error) {
            this.setState({store: store});
            this.setState({isStoreLoading: false})

        }
    };

    storeData = async () => {
        let storingValue = JSON.stringify(this.state.store.getState());
        try {
            await AsyncStorage.setItem('allData', storingValue)
        } catch (error) {
            console.log(error)
        }
    };

    componentWillMount() {
        AppState.addEventListener('change', this._handleAppStateChange)
        this.retrieveData();
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

     _handleAppStateChange = () => {
        this.storeData();
    }

    render() {
        if (!this.state.loaded) {
            return (
                <AppLoading
                    startAsync={loadFonts}
                    onError={() => console.log("error")}
                    onFinish={() => this.setState({loaded: true})}
                />
            )
        } else if (this.state.isStoreLoading) {
            return (<Text>Loading Store ...</Text>)
        } else {
            return (
                <Provider store={this.state.store}>
                    <Drawer/>
                </Provider>)
        }
    }
}
