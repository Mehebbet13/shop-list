import React, {useState,useEffect} from 'react';
import {TextInput, View} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import {CustomText} from "../components/CustomText";
import {styles} from "./CreateList";
import {connect} from "react-redux";
import {addUserCredentials, getCredentials} from "../store/project";
import {Button} from "../components/Button";
import COLORS from "../styles/colors";
import {Input} from "../components/Input";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as FileSystem from 'expo-file-system';


const mapStateToProps = state => ({
    credentials: getCredentials(state)
})
const getPermissions = async () => {
    try {
        const result = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL)
        console.log(result)

        if (result.status !== 'granted') {
            console.log('access denied')
            return false;
        }
        return true;
    } catch (e) {

    }

}



export const UserSettings = connect(mapStateToProps, {addUserCredentials})((props) => {
    const [username, setUsername] = useState();
    const [image, setImage] = useState();


    useEffect(() => {
        getPermissions().then(answer => {
            console.log('answer', answer)
        })
    }, [])

    const takeImageFromGallery = async () => {
        const image = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [1, 1],
        });
        if (!image.cancelled) {
            image.id = createID();
            delete image.cancelled;

            const newPath = `${FileSystem.documentDirectory}${image.uri.split('/').pop()}`;
            const result = await  FileSystem.moveAsync({
                from:image.uri,
                to:newPath
            })
            image.uri = newPath;
            console.log(result)
            setImage(image);
        }
    }
    const createAccount = () => {
        props.addUserCredentials({image,username});
        props.navigation.navigate('ONE TIME LIST');
    }

    return (
        <ScreensBackground title={'User Settings'}>

            <View style={{width: '100%'}}>
                <CustomText style={styles.listName}>username</CustomText>
            </View>

            <Input onChangeText={(text) => setUsername( text)}/>

            <View style={{width: '100%'}}>
                <CustomText style={styles.listName}>avatar url</CustomText>
            </View>



            <Button btnColor={COLORS.main}
                    btnWidth={'70%'}
                    btnName={'UPLOAD IMAGE'}
                    btnTextColor={COLORS.secondary}
                    fontSize={14}
                    onPress={takeImageFromGallery}
            />


            <Button btnColor={COLORS.main}
                    btnWidth={'90%'}
                    btnName={'SAVE CHANGES'}
                    btnTextColor={COLORS.secondary}
                    fontSize={14}
                    onPress={() => createAccount()}
            />

        </ScreensBackground>
    );
})
function createID() {
    return `${Math.random()} ${Date.now()}`;
}
