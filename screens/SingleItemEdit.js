import React, {useState,useEffect} from 'react';
import {TextInput, TouchableOpacity, Image, View, FlatList,Keyboard} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import back from '../assets/back.png';
import save from '../assets/save.png';
import edit from '../assets/edit.png';
import remove from '../assets/remove.png';
import {CustomText} from "../components/CustomText";
import {styles} from "./SingleListEdit";
import {getLists, updateListItem, deleteListItem} from "../store/project";
import {connect} from "react-redux";
import {Button} from "../components/Button";
import COLORS from "../styles/colors";

const mapStateToProps = state => ({
    lists: getLists(state)
})
export const SingleItemEdit = connect(mapStateToProps, {updateListItem, deleteListItem})((props) => {
    const options = ['pkg', 'kg', 'litre', 'bott'];

    const {listTitle, listId, id, isBought, isRegular,name,unit,amount} = props.route.params;
    const {lists} = props;

    const [unitIdx, setUnitIdx] = useState(options.indexOf(unit));
    const [updatedItems, setUpdatedItems] = useState({
        listId: listId,
        id: id,
        name: name,
        amount: amount,
        unit: unit,
        isBought: isBought
    });
    const indexOfList = lists.findIndex(
        (list) => {
            return list.id === updatedItems.listId
        }
    );
    const handleUpdate = () => {
        props.updateListItem({
            id: id,
            listId: listId,
            name: updatedItems.name,
            amount: updatedItems.amount,
            unit: updatedItems.unit,
            isBought: updatedItems.isBought,
        });
       Keyboard.dismiss();
    }
    const handleDeleteItem = (id) => {
        props.deleteListItem({
            listId: listId,
            id,
        });
    }
    const handleEdit = (id,nameEdit,unitEdit, amountEdit) => {
        props.navigation.navigate('SingleItemEdit', {
            listTitle: listTitle,
            listId: listId,
            id: id,
            isBought: false,
            isRegular: isRegular,
            name:nameEdit,
            unit:unitEdit,
            amount:amountEdit
        })
    }
    const handleListItems = (name, value) => {
        setUpdatedItems(items => ({
            ...items,
            [name]: value
        }))


        if (name === 'unit') {
            const idx = options.indexOf(value);
            setUnitIdx(idx);

        }
    }
    const handleCancel = () => {
        props.navigation.goBack();
    }
    return (
        <ScreensBackground title={listTitle}
                           image1={back}
                           image2={save}
                           onPress1={() => props.navigation.goBack()}
                           onPress2={() => props.navigation.navigate('SingleList', {
                               listTitle: listTitle,
                               listId: listId,
                               indexOfList: indexOfList,
                               isRegular: isRegular

                           })}
        >
            <View style={styles.addingProductSection}>
                <View>
                    <CustomText style={styles.positionName}>position name</CustomText>
                    <View style={styles.textInput}>
                        <TextInput
                            defaultValue={name}
                            onChangeText={(text) => handleListItems('name', text)}/>
                    </View>
                </View>
                <View>
                    <CustomText style={styles.count}>count</CustomText>
                    <View style={styles.counterContainer}>
                        <TouchableOpacity style={styles.minus}
                                          onPress={(count) => handleListItems('amount', updatedItems.amount < 1  || isNaN(updatedItems.amount)? 0 : updatedItems.amount - 1)}>
                            <CustomText weight={'bold'} style={{fontSize: 27}}>-</CustomText>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <CustomText weight={'bold'} style={{fontSize: 16}}>{updatedItems.amount}</CustomText>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => handleListItems('amount', updatedItems.amount + 1)}>
                            <CustomText weight={'bold'} style={{fontSize: 20}}>+</CustomText>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.options}>
                    {options.map((option, ind) => (
                        <View style={[styles.optionBtn, {opacity: (unitIdx === ind) ? 1 : 0.5}]}>
                            <TouchableOpacity onPress={() => handleListItems('unit', option)}>
                                <CustomText weight={'bold'}>{option}</CustomText>
                            </TouchableOpacity>
                        </View>
                    ))}
                </View>
                <View style={styles.btnContainer}>
                    <View>
                        <Button btnColor={COLORS.main}
                                btnWidth={140}
                                btnName={'CANCEL'}
                                btnTextColor={COLORS.secondary}
                                fontSize={14}
                                onPress={() => handleCancel()}
                        />
                    </View>
                    <View style={{marginLeft: 10}}>
                        <Button btnColor={COLORS.main}
                                btnWidth={140}
                                btnName={'UPDATE'}
                                btnTextColor={COLORS.secondary}
                                fontSize={14}
                                onPress={() => handleUpdate()}
                        />
                    </View>
                </View>
            </View>
            <FlatList
                data={lists[indexOfList].listProducts}
                // inverted={true}
                renderItem={({item}) =>
                    <View style={[styles.item, {opacity: 1}]}>
                        <View style={[styles.edit, {paddingBottom: 27,opacity: item.id === id ? 0.4 : 1}]}>
                            <TouchableOpacity style={styles.edit}
                                              activeOpacity={0.7}
                                              onPress={item.id === id ?
                                                  () => alert(`now you are updating this item`) :
                                                  () => handleEdit(item.id,item.name,item.unit,item.amount)}
                            >
                                <Image source={edit}/>
                            </TouchableOpacity>
                        </View>
                        <CustomText style={styles.productName}>{item.name}</CustomText>
                        <CustomText style={styles.productAmount}>x{item.amount} {item.unit}</CustomText>
                        <TouchableOpacity style={styles.remove}
                                          activeOpacity={0.7}
                                          onPress={() => handleDeleteItem(item.id)}
                        >
                            <Image source={remove}/>
                        </TouchableOpacity>
                    </View>
                }
                keyExtractor={item => item.id}
            />


        </ScreensBackground>
    );
});
