import React, {useState} from 'react';
import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import edit from '../assets/edit.png';
import back from '../assets/back.png';
import {CustomText} from "../components/CustomText";
import {getLists, toggleBought, resetBoughtItem} from "../store/project";
import {connect} from "react-redux";
import COLORS from "../styles/colors";

const mapStateToProps = state => ({
    lists: getLists(state)
})

export const SingleList = connect(mapStateToProps, {toggleBought, resetBoughtItem})((props) => {
    const {indexOfList} = props.route.params;
    const {lists} = props;
    const {listTitle} = props.route.params;
    const {listId} = props.route.params;
    const {isRegular} = props.route.params;
    const handleBoughtItem = (name, id) => {
        props.toggleBought({
            listId,
            id,
        });
    }
    const boughtItems = lists[indexOfList].listProducts.filter(item => item.isBought === true);
    const boughtItemsSize = boughtItems.length;
    const allItemsSize = lists[indexOfList].listProducts.length;
    const handleReset = () => {
        if (isRegular) {
            props.resetBoughtItem({listId: listId});

        }
    }

    return (
        <ScreensBackground title={listTitle}
                           image2={edit}
                           image1={back}
                           onPress1={() => props.navigation.goBack()}
                           onPress2={() => props.navigation.navigate('SingleListEdit')}>
            <View style={styles.resetBtnContainer}>
                {isRegular ?
                    <TouchableOpacity activeOpacity={0.5} style={styles.resetBtn} onPress={() => handleReset()}>
                        <CustomText weight={'bold'} style={styles.resetBtnName}>
                            RESET
                        </CustomText>
                    </TouchableOpacity>
                    : null}
                <CustomText weight={'medium'}
                            style={styles.listItemPercentage}>{boughtItemsSize}/{allItemsSize}</CustomText>
            </View>
            <FlatList
                data={lists[indexOfList].listProducts}
                renderItem={({item}) =>
                    <View style={{opacity: (item.isBought) ? 0.4 : 1}}>
                    <TouchableOpacity style={styles.item}
                                      onLongPress={() => handleBoughtItem('isBought', item.id)}
                                      onPress={item.isBought?() => alert('You already have bought it!')
                                          :() => alert('Press and hold for setting as bought!')}
                    >
                        <CustomText style={styles.productName}>{item.name}</CustomText>
                        <CustomText style={styles.productAmount}>x{item.amount} {item.unit}</CustomText>
                    </TouchableOpacity>
                    </View>
                }
                keyExtractor={item => item.id}
            />
        </ScreensBackground>
    );
})
const styles = StyleSheet.create({
    resetBtnContainer: {
        width: '100%',
        marginBottom: 50
    },
    resetBtnName: {
        fontStyle: 'normal',
        fontSize: 12,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#000000'
    },
    resetBtn: {
        width: 100,
        padding: 5,
        backgroundColor: COLORS.main,
        borderRadius: 20,
        position: 'absolute',
        top: 10,
        left: 20,
        marginBottom: 30,
        textAlign: 'center'
    },
    listItemPercentage: {
        position: 'absolute',
        top: 18,
        right: 20,
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        color: COLORS.black,
        letterSpacing: 2,
    },
    item: {
        width: 300,
        backgroundColor: COLORS.secondary,
        borderRadius: 45,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 20,
        position: 'relative',
        borderWidth: 2,
        borderColor: COLORS.lightYellow,

    },
    productName: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        position: 'absolute',
        bottom: 7,
        left: 15,
        color: COLORS.black,
    },
    productAmount: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        position: 'absolute',
        bottom: 7,
        right: 15,
        color: COLORS.black,
    }

});
