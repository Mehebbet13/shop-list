import React, {useState,useEffect} from 'react';
import {StyleSheet, FlatList,AsyncStorage} from 'react-native';
import {ScreensBackground} from "../../components/ScreensBackground";
import sidebar from '../../assets/sidebar.png';
import {connect} from "react-redux";
import {deleteList, getLists} from "../../store/project";
import {SingleItem} from "../../components/SingleItem";
import ModalComp from "../../components/Modal";
import COLORS from "../../styles/colors";

const mapStateToProps = state => ({
    lists: getLists(state)
})
export const HomeScreen = connect(mapStateToProps,{deleteList})((props) => {
    const {lists} = props;
    let OneTimeLists=lists.filter((regularList)=>regularList.isRegular===false);
    const [isModalOpen, setModalOpen] = useState(false);
    const [id, setId] = useState('');


    const handleDeleteList = (listId) => {
        props.deleteList({
            listId: listId,
        });
        hideModal();
    }
    const hideModal=()=>{
        setModalOpen(!isModalOpen);
    };
    const openModal=(id)=>{
        setModalOpen(!isModalOpen);
        setId(id);
    };
    return (
        <>
        <ScreensBackground title={'One Time Lists'}
                           image2={sidebar}
                           onPress2={() => props.navigation.openDrawer()}>
            <FlatList
                data={OneTimeLists}
                renderItem={({item}) =>
                    <SingleItem
                                key={item.id}
                                listId={item.id}
                                isRegular={item.isRegular}
                                onLongPress={()=>openModal(item.id)}
                                boughtItemsSize={item.listProducts.filter((product)=>product.isBought===true).length}
                                listTitle={item.title}
                                listProductsLength={item.listProducts.length}
                                onPress={(listId,isRegular,listTitle) =>
                                    props.navigation.navigate("SingleListEdit",
                                        { listTitle:listTitle,
                                            listId: listId,
                                            isRegular:isRegular,
                                        })
                                }

                    />
                }
                keyExtractor={item => `${item.id}${Date.now()}`}
            />

        </ScreensBackground>
            { isModalOpen?
                <ModalComp hideModal={()=>hideModal()}  handleDeleteList={()=>handleDeleteList(id)}/>
                :null
            }
            </>
    );
})

export const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor:COLORS.main,

    },
    pageContentNameContainer: {
        height: 120,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    pageContentName: {
        fontStyle: 'normal',
        fontSize: 16,
        lineHeight: 20,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color:COLORS.secondary,
        marginTop: 30

    },
    listItemTitle: {
        fontStyle: 'normal',
        fontSize: 18,
        lineHeight: 20,
        color:COLORS.black,
        marginTop: 10,
        marginLeft: 5,
    },
    listItemPercentage: {
        position: 'absolute',
        top: 11,
        right: 5,
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        color: '#303234',
        letterSpacing: 2,
    },
    pageContent: {
        height: 600,
        width: '100%',
        backgroundColor:COLORS.secondary,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        paddingBottom: 50

    },
    listItem: {
        width: 300,
        height: 90,
        borderWidth: 2,
        borderColor: '#FFE194',
        borderRadius: 14,
        marginTop: 20,
    },
    image2: {
        width: 20,
        height: 20,
        position: 'absolute',
        right: 20,
        top: 65

    },
    image1: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 15,
        top: 65

    },
    progressBar: {
        position: 'absolute',
        top: 40,
        left: 15,
        height: 18,
        width: '90%',
        backgroundColor:COLORS.gray,
        borderRadius: 20
    }
});
