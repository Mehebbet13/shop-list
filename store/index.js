import {createStore, combineReducers} from 'redux';
import {projectsReducer} from "./project";


const rootReducers = combineReducers({
    projects:projectsReducer
});


const store = createStore(
    rootReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );


export default store;
